# API

It is possible to control classification over REST api. For now only user access with a valid authorization token is supported. Access of external application is being worked on.

Documentation with an interface to try calls is here: https://grades.fit.cvut.cz/api/v1/swagger-ui.html

Before entering the page **it is necessary to be logged in in classification**, otherwise due to a bug in the system the page might not work. Fixed in an upcomming version.

>###**!!! Keep in mind that the inferface is not final and will change !!!**

# Výrazy

Výrazy pro sloupce se funkcionalitou

* z bodů udělat známku -- `MARK(body)`
* ukázka jednoduchého výrazu -- `(body + test) >= 20`
* sečíst mnoho sloupců -- ``SUM(`sloupec\d+`)``
* sečíst počet výskytů 'N' v mnoha sloupcích -- ``COUNT('N',`sloupec\d+`)``
* z pokusů vybrat poslední -- ``LAST(`pokus\d+`)`` *řadí abecedně dle identifikátoru!*

## Formální specifikace výrazů

* Syntaxe výrazů je z velké části založena na syntaxi jazyka C
* Jazyk je **silně typovaný**. Při vytváření definice hodnocení se kromě unikátního identifikátoru volí i datový typ
* Datové typy jsou:
    * číslo (vnitřně *double*).
    * řetězec.
    * boolean.
* **Identifikátory** proměnných a funkcí, a klíčová slova jsou **case insensitive**.
* Nedefinovaná hodnota je *null*. V aritmetických a logických operacích (kromě porovnání na rovnost) se chová jako 0, prázdný řetězec nebo false
* Na **pořadí výrazů nezáleží**, ve výrazech se ale **nesmí nacházet cyklické závislosti** proměnných.
* **Nesprávné výrazy** se automaticky **vyhodnocují na null**.

## Operátory

<table>
    <tr>
        <th>priorita</th>
        <th>Operátor</th>
        <th>Arita</th>
        <th>Syntaxe</th>
        <th>Význam</th>
    </tr>
    <tr>
        <td>max</td>
        <td>!</td>
        <td>unární</td>
        <td>!boolean</td>
        <td>logická negace</td>
    </tr>
    <tr>
        <td></td>
        <td>-</td>
        <td>unární</td>
        <td>-číslo</td>
        <td>unární mínus</td>
    </tr>
    <tr>
        <td></td>
        <td>+</td>
        <td>unární</td>
        <td>+číslo</td>
        <td>unární plus</td>
    </tr>
    <tr>
        <td></td>
        <td>^</td>
        <td>binární</td>
        <td>číslo^číslo</td>
        <td>mocnění</td>
    </tr>
    <tr>
        <td></td>
        <td>* / %</td>
        <td>binární</td>
        <td>číslo * číslo</td>
        <td>násobení, dělení, modulo</td>
    </tr>
    <tr>
        <td></td>
        <td>+ -</td>
        <td>binární</td>
        <td>číslo + číslo<br>řetězec + řetězec</td>
        <td>sčítání, odčítání<br>zřetězení</td>
    </tr>
    <tr>
        <td></td>
        <td>> < >= <=</td>
        <td>binární</td>
        <td>číslo > číslo</td>
        <td>porovnání</td>
    </tr>
    <tr>
        <td></td>
        <td>== !=</td>
        <td>binární</td>
        <td>výraz == výraz</td>
        <td>rovnost, nerovnost. Lze porovnávat na null</td>
    </tr>
    <tr>
        <td></td>
        <td>&&</td>
        <td>binární</td>
        <td>boolean && boolean</td>
        <td>logické and</td>
    </tr>
    <tr>
        <td></td>
        <td>||</td>
        <td>binární</td>
        <td>boolean || boolean</td>
        <td>logické or</td>
    </tr>
    <tr>
        <td>min</td>
        <td>?:</td>
        <td>ternární</td>
        <td>boolean ? výraz : výraz</td>
        <td>pokud je pravdivá podmínka, pak první výraz, jinak druhý</td>
    </tr>
</table>

Kulaté závorky upravují prioritu operátorů

## Funkce

<table>
    <tr>
        <th>Funkce</th>
        <th>Návratový typ</th>
        <th>Počet parametrů</th>
        <th>Syntaxe</th>
        <th>Význam</th>
    </tr>
    <tr>
        <td>MIN</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>MIN(číslo, ...)</td>
        <td>minimum</td>
    </tr>
    <tr>
        <td>MAX</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>MAX(číslo, ...)</td>
        <td>maximum</td>
    </tr>
    <tr>
        <td>SUM</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>SUM(číslo, ...)</td>
        <td>suma</td>
    </tr>
    <tr>
        <td>CONCAT</td>
        <td>řetězec</td>
        <td>variabilní</td>
        <td>CONCAT(řetězec, ...)</td>
        <td>zřetězení</td>
    </tr>
    <tr>
        <td>PRODUCT</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>PRODUCT(číslo, ...)</td>
        <td>produkt</td>
    </tr>
    <tr>
        <td>OR</td>
        <td>boolean</td>
        <td>variabilní</td>
        <td>OR(boolean, ...)</td>
        <td>logické or</td>
    </tr>
    <tr>
        <td>AND</td>
        <td>boolean</td>
        <td>variabilní</td>
        <td>AND(boolean, ...)</td>
        <td>logické and</td>
    </tr>
    <tr>
        <td>ROUND</td>
        <td>číslo</td>
        <td>1<br>2</td>
        <td>ROUND(číslo)<br>ROUND(číslo, číslo)</td>
        <td>standardní zaokrouhlení na celá čísla<br>standardní zaokrouhlení na daný počet desetinných míst</td>
    </tr>
    <tr>
        <td>FLOOR</td>
        <td>číslo</td>
        <td>1</td>
        <td>FLOOR(číslo)</td>
        <td>dolní celá část</td>
    </tr>
    <tr>
        <td>CEIL</td>
        <td>číslo</td>
        <td>1</td>
        <td>CEIL(číslo)</td>
        <td>horní celá část</td>
    </tr>
    <tr>
        <td>MARK</td>
        <td>řetězec</td>
        <td>1</td>
        <td>MARK(číslo)</td>
        <td>známka z počtu bodů podle studijního řádu ČVUT</td>
    </tr>
    <tr>
        <td>LAST</td>
        <td>typ</td>
        <td>variabilní</td>
        <td>LAST(typ, ...)</td>
        <td>poslední definovaná hodnota</td>
    </tr>
    <tr>
        <td>VALID</td>
        <td>boolean</td>
        <td>variabilní</td>
        <td>VALID(typ, ...)</td>
        <td>pravda pokud všechny argumety jsou definované</td>
    </tr>
    <tr>
        <td>COUNT</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>COUNT(typ, ...)</td>
        <td>kolik hodnot je stejných jako první argument</td>
    </tr>
    <tr>
        <td>ITH</td>
        <td>typ</td>
        <td>variabilní</td>
        <td>ITH(číslo, ...)</td>
        <td>vrátí i-tý definovaný prvek, jinak null</td>
    </tr>
    <tr>
        <td>LASTIDX</td>
        <td>číslo</td>
        <td>variabilní</td>
        <td>LASTIDX(typ, ...)</td>
        <td>index posledního definovaného argumentu</td>
    </tr>
</table>

Dále je možné používat matematické funkce *ABS, SQRT, LOG, LOG10, SIN, COS,
    TAN, ASIN, ACOS, ATAN, SINH, COSH* a *TANH*.
    
Ke každému operátoru též existuje ekvivalentní funkce. Kromě již zmíněných to jsou *IF, EQ, NEQ, GT, GTE, LT, LTE, MINUS, DIV, MOD, POW, UPLUS, UMINUS* a *NEG*.

## Argumenty funkcí dané regulárním výrazem

Ve výrazech lze zapsat argumenty funkcí pomocí regulárních výrazů.
Vyhledávání probíhá na identifikátorech definovaných polí hodnocení a je <strong>case insensitive</strong>.
Regulární výrazy lze použít pouze jako parametry funkcí a jsou uvozeny zpětným apostrofem (znakem <strong>`</strong>, ascii 96).

Pokud máme například definovány body za aktivitu pro každé cvičení pojmenované
<code>ac1, ac2, ..., ac13</code>, lze potom
všechny tyto hodnoty sečíst takto: <code>SUM(`ac\d+`)</code> .
Podobně jdou regulární výrazy použít i u ostatních funkcí s variabilním počtem argumentů.

Ve zkratce:
<table>
    <tr>
        <td>x</td>
        <td>znak 'x'</td>
    </tr>
    <tr>
        <td>[abc]</td>
        <td>znak 'a', 'b' nebo 'c'</td>
    </tr>
    <tr>
        <td>\d</td>
        <td>číslo</td>
    </tr>
    <tr>
        <td>x+</td>
        <td>opakování jednou a více krát</td>
    </tr>
    <tr>
        <td>x*</td>
        <td>opakování nula a více krát</td>
    </tr>
    <tr>
        <td>xyz|abc</td>
        <td>xyz nebo abc</td>
    </tr>
</table>
Vyčerpávající dokumentace regulárních výrazů je dostupná v
<a href="https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html">
    dokumentaci třídy java.util.regex.Pattern.
</a>

<table>
    <tr>
        <th>Výraz</th>
        <th>Přijaté argumenty</th>
        <th>Odmítnuté argumenty</th>
    </tr>
    <tr>
        <td>test\d+</td>
        <td>test1, test2, ..., test12</td>
        <td>test, testC</td>
    </tr>
    <tr>
        <td>f1*</td>
        <td>f, f1, f111111</td>
        <td>f2, 1f</td>
    </tr>
    <tr>
        <td>(hw|test)_\w*</td>
        <td>hw_a, hw_b, test_midterm</td>
        <td>hw, test_12</td>
    </tr>
</table>


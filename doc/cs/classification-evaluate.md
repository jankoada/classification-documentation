# Hodnocení studentů
V liště vlevo máte seznam svých předmětů.
U každého můžete nastavit, jak je předmět hodnocen (Definice hodnocení).
Po vytvoření definice je možné hodnotit studenty několika různými způsoby.

* chcete zadávat více hodnocení menším skupinám -- použijte *Hromadné hodnocení*
* chcete zapsat jednu položku velkému počtu studentů -- *Podrobné hodnocení*
* chcete hodnotit jednoho studenta -- *Hodnocení studenta*
* chcete přidat k hodnocení komentář pro studenta -- *Podrobné hodnocení* nebo *Hodnocení studenta*
* chcete hodnotit spoustu věcí spoustě lidem -- *export / import* ve formátu csv

## Import / Export CSV

Tabulku hodnocení lze ze stránky *Hodnocení přemětu* exportovat a importovat pomocí tlačítek *Export do csv* a *Nahrát csv*.
Tento formát lze editovat například v programech *Microsoft Excel, LibreOffice Calc* či v libovolém *textovém editoru*.

* podporujeme běžné oddělovače -- čárka, středník
* importujte libovolnou podmnožinu studentů i sloupců, ostatní záznamy nebudou změněny
* při importu se naimportují pouze ti studenti, kteří jsou ve vybrané skupině, ostatní ignorujem
* pokud import obsahuje prázdný záznam, tak se při importu hodnocení **přemaže** příslušné hodnocení
* po importu **je nutné uložit**

# Dokumentace definice hodnocení předmětu
## Vytvoření definice

* **Název česky** - Český název hodnocení
* **Název anglicky** - Anglický název hodnocení
* **Identifikátor** - Identifikátor slouží jako proměnná pro dané hodnocení. Používá se v hodnocení, které se vypočítá podle zadaného vzorce. Příklad zadávání vzůrců najdete níže.
* **Vztah k předmětu** - Vztah k předmětu slouží jako metadata k danému hodnocení. Není příliž podstatné k funkci hodnocení, ale pomůže tvoření statistik hodnocení předmětů.
* **Datový typ** - Vzhledem k tomu, že hodnocení podporuje zadávání vzorců, je potřeba ke každé proměnné definovat její datový typ.
* **Způsob zadání** - Způsob zadání definuje, jestli hodnotu budou zadávat vyučující nebo se vypočítá sama z ostatních hodnocení.
* **Viditelnost pro studenty** - V některých případech si vyučující potřebuje ke studentům udělat
        'soukromou'
        poznámku. Další možnost je mezivýpočet v hodnocení, který je zbytečné ukazovat studentům, ale výrazně zjednoduší
        následující vzorce. Toto hodnocení uvidí všichni učitelé předmětu.
* **Minimální požadovaná hodnota** - Minimální požadovaná hodnota ukazuje studentům, jaké je pro
        číselné
        hodnocení požadované minimum. Není to povinná hodnota. Pokud je student ohodnocen hodnocením menším než je
        minimum, je na to učitel i student graficky upozorněn. Minimální požadova hodnota nijak nezasahuje do udělování
        zápočtů nebo známky, tato podmínka musí být v příslušném vzorci definovaná znovu.
* **Maximální hodnota** - Maximální požadovaná hodnota ukazuje studentům, kolik lze získat z
        hodnocení
        maximum bodů. Není to povinná hodnota. Pokud je student ohodnocen hodnocením větším než je maximum, je na to
        učitel graficky upozorněn (Zadávání většího hodnocení je povoleno).

<img alt="Nový sloupec hodnocení" src="https://gitlab.fit.cvut.cz/evolution-team/classification-documentation/raw/master/img/new_column.png"/>


## Vzorce

Podrobnosti k vytváření vzorců jsou více rozvedeny v sekci "Tvorba vzorců". Na následujícím příkladu je
demonstrována
funkce takových vzorců. Důležité je, aby výsledek vzorce ctil datový typ daného hodnocení.

V následujícím příkladu existuje předmět. V tomto předmětu se píší 3 testy v semestru. Každý test má minimumální
požadavek na zápočet 5 bodů. Existují dva domácí úkoly, každý má požadavek na zápočet opět 5 bodů.
Pro splnění zápočtu musí žák splnit podmínky zápočtu a získat ze semestru alespoň 25 bodů.
Při splnění zápočtu se píše zkouškový test. Minimální hodnota na získání známky z testu je 25 bodů.
Při splnění podmínek pro zápočet a pro získání známky je student hodnocen podle standardní stupnice ČVUT: (∞,50) -
F, <50-60) - E, <60-70) - D, <70-80) - C, <80-90) - B, <90,∞) - A.

V příkladu můžeme vidět použití regulárních výrazů (`test\d+` = test1,test2,test3), funkcí, proměnných, operátorů.
Každý vzorec je v reálném čase kontrolován a uživatel je informován o jeho validnosti. Zároveň může využít nápovědy,
která se snaží napovědět, kde se případná chyba nachází.

<img alt="Vzorec" src="https://gitlab.fit.cvut.cz/evolution-team/classification-documentation/raw/master/img/formula.png"/>

## Import definice hodnocení

Definovat zadání je možno také importováním zadání z jiného předmětu/semestru. Této funkce se dá vyžít k importu
definic hodnocení z minulého semestru, popřípadě z anglické/české/kombinované verze předmětu. Při importování se
volí možnost přepsání stávajících hodnocení. Nepředpokládá se, že by se import prováděl v době, kdy už nejaké
hodnocení existují, ale je nutné toto ošetřit. Možnost smazání stávajících zadání smaže všechny definice i
studentská hodnocení v současném semestru a nahradí je importem, možnost nesmazání zachová stávající hodnocení a
pouze přidá ty, které nemají shodný identifikátor s již existujícími.

Importovat lze pouze z předmětů a semestrů, které má učitel právo vidět - byl v zadaném semestru a předmětu
vyučující.

<img alt="Vzorec" src="https://gitlab.fit.cvut.cz/evolution-team/classification-documentation/raw/master/img/import_definition.png"/>

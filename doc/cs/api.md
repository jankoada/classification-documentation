# API

Klasifikaci je možné ovládat přes REST api. Prozatím je podporován pouze uživatelský přístup s platným autorizačním tokenem. Přístup externích aplikací se chystá.

Dokumentace s rozhraním pro vyzkoušení dotazů je zde: https://grades.fit.cvut.cz/api/v1/swagger-ui.html

Před vstupem na stránku **je potřeba být přihlášen v klasifikaci**, jinak z důvodu chyby v systému stránka nemusí fungovat. Opraveno v následující verzi.

> ###**!!! Berte na vědomí, že rozhraní api není finální a bude se ještě měnit !!!**

# Changelog aplikace Klasifikace

## Březen 2018

* Zaveden changelog
* Přidána skupina umělých studentů pro testování hodnocení
* Hromadné vytváření definic hodnocení
* Push notifikace
* Okamžité zobrazení nového ohodnocení studentům bez obnovení stránky
* Možnost pro učitele vidět hodnocení z pohledu studenta (odkaz u jména studenta v tabulce hodnocení)
* Stránkování tabulky hodnocení
* Možnost poslat upozornění aktuálně vybraným studentům v hodnocení předmětu
* Filtrování sloupců v tabulce hodnocení pro semestr nebo zkouškové (rozpoznáváno podle zvoleného vztahu k předmětu)
* Přidána jména učitelů k paralelkám

## Únor 2018
